Dado('que foi definido o endpoint {string}') do |endpoint|
  $uri_base = endpoint
end

Quando('pesquiso name {string}') do |string|
  $response = HTTParty.get($uri_base, query: {name: string})
end

Então('a API irá retornar os dados respondendo o código {int}') do |int|
  expect($response.code).to eq(int)
end

Quando('realizo um post de um novo elemento') do
  $response = ApiHelper.new.post_value($uri_base)
end

Então('o id do elemento adicinado') do
  expect($response['id']).to eq(11)
end

Quando('realizo um put no usuário {int}') do |int|
  $response = ApiHelper.new.put_value($uri_base)
end

Então('será alterado os valores do usuário') do
  ApiHelper.new.valid_put($response['id'])
end
