require "httparty"
require "faker"

class ApiHelper
  include HTTParty

  @new_value = {
    "name": Faker::Name.name,
    "username": Faker::Internet.username,
    "email": Faker::Internet.email,
    "address": {
      "street": Faker::Address.street_name ,
      "suite": Faker::Address.secondary_address ,
      "city": Faker::Address.city,
      "zipcode": Faker::Address.zip_code,
      "geo": {
        "lat": Faker::Address.latitude,
        "lng": Faker::Address.longitude
      }
    },
    "phone": Faker::PhoneNumber.cell_phone,
    "website": Faker::Internet.url,
    "company": {
      "name": Faker::Company.name,
      "catchPhrase": Faker::Company.catch_phrase ,
      "bs": Faker::Company.bs
    }
  }
  @update_value = {
    "name": Faker::Name.name,
    "username": Faker::Internet.username,
    "email": Faker::Internet.email,
    "website": Faker::Internet.url,
  }
  def post_value(value)

    self.class.post(value,:headers => { 'Content-Type' => 'application/json' }, :body => @new_value)
  end

  def put_value(value)
    self.class.put(value,:headers => { 'Content-Type' => 'application/json' }, :body => @update_value)
  end

  def valid_put(id)
    self.class.put(value, :body => @update_value)
  end
end
