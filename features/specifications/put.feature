#language: pt
@put_api
Funcionalidade: PUT no endpoint
  Realizar um put no endpoint https://jsonplaceholder.typicode.com/users alterando os valores
  dos campos: email, lat e lng do usuário com id = 5 e Validar o statuscode = 200 e os dados alterados

  Cenário: Alterar valor do usuário 5
    Dado que foi definido o endpoint "https://jsonplaceholder.typicode.com/users"
    Quando realizo um put no usuário 5
    Então a API irá retornar os dados respondendo o código 200
    E será alterado os valores do usuário