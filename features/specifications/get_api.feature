#language: pt
@get_api
Funcionalidade: Get no endpoint
  Realizar uma requisição no endpoint https://jsonplaceholder.typicode.com/comments
  pesquisando pelo atributo name: alias odio sit

    Cenário: Pesquisar atributo name
      Dado que foi definido o endpoint "https://jsonplaceholder.typicode.com/comments"
      Quando pesquiso name "alias odio sit"
      Então a API irá retornar os dados respondendo o código 200