#language: pt
@post_api
Funcionalidade: POST no endpoint
  Realizar um post no endpoint https://jsonplaceholder.typicode.com/users enviando os valores
  no body e validar o statuscode = 201 e o id retornado

  Cenário: Adicionar novo usuário
    Dado que foi definido o endpoint "https://jsonplaceholder.typicode.com/users"
    Quando realizo um post de um novo elemento
    Então a API irá retornar os dados respondendo o código 201
    E o id do elemento adicinado